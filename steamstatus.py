#!/bin/env python3
# -*- fill-column: 120; indent-tabs-mode: nil; tab-width: 10; python-indent-offset: 2 -*-

from . import core
from .core import StatsModule, StatsDaemon, StatsModuleAPI

import psutil
import datetime
import os
import os.path
import time
import re

STEAM_CONNECTION_LOG = os.path.join(os.path.expanduser("~"), ".steam", "root", "logs", "connection_log.txt")

# Looks like
#   [2020-02-18 17:27:06] [0,0] SetSteamID( [U:1:12345] )
# Captures (steamtimestamp, universe, accountID)
USER_CONNECT_LOG_LINE = re.compile("^(\[[0-9]{4}-[0-9]{2}-[0-9]{2} (?:[0-9]{2}:){2}[0-9]{2}\]) \[[^\]]+\] SetSteamID\( *\[U:([0-9]+):([0-9]+)\] *\)")

class steamstatus(StatsModule):
  def __init__(self, daemon: StatsDaemon, api: StatsModuleAPI):
    self.daemon = daemon
    self.api = api
    self.monitor_process = None
    self.cached_game = None
    self.steam_connection_log = None
    self.steam_connection_log_stat = None
    self.steam_connection_log_fragment = ""
    self.high_water_time = self.api.persist_get("high_water_time") or 0.0

    self.check_open_steam_log()
    self.api.log(f"Initialized steamstatus - high water time {self.high_water_time}")

  def stat_steam_log(self):
    try:
      return os.stat(STEAM_CONNECTION_LOG)
    except:
      return None

  def check_open_steam_log(self):
    # Check if we need to re-open the file -- Make sure dev/ino is the same as our open file.
    if self.steam_connection_log:
      newstat = self.stat_steam_log()
      if not newstat \
         or self.steam_connection_log_stat.st_ino != newstat.st_ino \
         or self.steam_connection_log_stat.st_dev != newstat.st_dev:
        self.steam_connection_log.close()
        self.steam_connection_log = None
        self.steam_connection_log_stat = None
        self.steam_connection_log_fragment = ""
        self.api.log("Closing connection log - replaced on disk")
    if not self.steam_connection_log:
      log = None
      stat = None
      try:
        log = open(STEAM_CONNECTION_LOG, "r", encoding="utf-8")
        stat = os.stat(log.fileno())
      except Exception as e:
        self.api.verbose(f"Failed to open steam connection log, {type(e)}: {e}")
      if log and stat:
        self.api.log("Opened connection log")
        self.steam_connection_log = log
        self.steam_connection_log_stat = stat

  def get_new_steam_log_lines(self):
    self.check_open_steam_log()
    if not self.steam_connection_log:
      return []
    text = self.steam_connection_log_fragment + self.steam_connection_log.read()
    loc = text.rfind('\n')
    lines = []
    if loc != -1:
      newtext = text[:loc]
      self.steam_connection_log_fragment = text[loc+1:]
      lines = newtext.strip().split("\n")
    else:
      self.steam_connection_log_fragment = text

    return lines

  def get_new_steam_logins(self):
    newlines = self.get_new_steam_log_lines()
    ret = []
    for line in newlines:
      match = re.match(USER_CONNECT_LOG_LINE, line.strip())
      if not match or len(match.groups()) != 3:
        continue

      ltime = self.parse_steam_time(match.group(1))
      # Reject lines more than 30s in the future in case of clock screwiness so we don't lock on to some insane
      # timestamp as highwatermark.
      if not ltime or ltime > time.time() + 30.0:
        self.api.verbose(f"Bad steam connection log line (time): {line}")
        continue

      universe = int(match.group(2))
      accountid = int(match.group(3))

      # This is the anonymous no-user login the log gets spammed with
      if accountid == 0:
        continue

      # If we re-opened the file or restarted just skip over lines before our watermark
      # (note that this lets us read times in descending order from a new logfile or some such)
      if ltime <= self.high_water_time:
        continue

      self.high_water_time = ltime

      # New line
      ret.append({ "time": ltime, "universe": universe, "accountid": accountid })
    # Found results, persist high water time
    if len(ret):
      self.api.persist_set("high_water_time", self.high_water_time)
    return ret

  # Helper to convert steam log times to timestamps
  #    Ex: [2020-02-18 14:54:38] -> 1582066478.0
  def parse_steam_time(self, steamtimestr):
    try:
      return datetime.datetime.strptime(steamtimestr, "[%Y-%m-%d %H:%M:%S]").timestamp()
    except ValueError:
      return None

  def find_steam_child_monitor(self):
    for p in psutil.process_iter(attrs=['name']):
      if p.info['name'] == "reaper":
        return p

  # Returns a dict of one or more { app_running: app_start: app_start: } based on state now and since last call
  def check_running_steam_app_events(self):
    # If we found a game and its child-monitor is still running, no need to re-find
    if self.cached_game and self.monitor_process and self.monitor_process.is_running():
      return { "app_running": self.cached_game }

    # No running game.  Emit stop if we had one
    ret = {}
    if self.cached_game:
      ret['app_stop'] = self.cached_game
      self.cached_game = None
      self.monitor_process = None

    # Check for one running
    child_monitor = self.find_steam_child_monitor()
    if child_monitor:
      for child in child_monitor.children():
        env = child.environ()
        if 'SteamAppId' in env:
          # Found one
          self.cached_game = env['SteamAppId']
          self.monitor_process = child_monitor
          # Emit start & running for the new one
          ret['app_start'] = self.cached_game
          ret['app_running'] = self.cached_game
          break

    return ret

  def collect(self):
    events = self.check_running_steam_app_events() or {}
    return [ *({ "operation": event, "appid": appid } for event,appid in events.items()),
             *({ "operation": "login", **login } for login in self.get_new_steam_logins()) ]
