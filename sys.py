#!/bin/env python3
# -*- fill-column: 120; indent-tabs-mode: nil; tab-width: 10; python-indent-offset: 2 -*-

# Collects basic system stats

from . import core
from . import util
from .core import StatsModule, StatsDaemon, StatsModuleAPI

import re
import time
import os.path
import json

RE_SPACES = re.compile("\s+")

# /proc/stat CPU columns from man 5 proc
PROC_COLUMNS = ["user", "nice", "system", "idle", "iowait", "irq", "softirq", "steal", "guest", "guest_nice"]

# Nearly every usb mouse and other thing will give off battery data, for now we just report the first battery found in
# this list
BATTERIES = [ "/sys/class/power_supply/BAT0",
              "/sys/class/power_supply/BAT1" ]

# Nodes to read and the function to parse them
def _BATTERY_INT(dat):
  try: return int(dat)
  except: return 0
def _BATTERY_STR(dat):
  try: return str(dat).strip()
  except: return ""

BATTERY_FIELDS = {
  "capacity":           _BATTERY_INT,
  "capacity_level":     _BATTERY_STR,
  "charge_full":        _BATTERY_INT,
  "charge_full_design": _BATTERY_INT,
  "charge_now":         _BATTERY_INT,
  "current_now":        _BATTERY_INT,
  "cycle_count":        _BATTERY_INT,
  "voltage_min_design": _BATTERY_INT,
  "voltage_now":        _BATTERY_INT,
  "status":             _BATTERY_STR
}

# /proc/self/mountinfo fields, split from before-dynamic-fields and after-dynamic-fields, see man 5 proc
MOUNTINFO_FIELDS_START = [
  "mountid",
  "parentid",
  "st_dev",
  "root",
  "mountpoint",
  "mountoptions"
]
# -- optional fields and separator occur between start and end --
MOUNTINFO_FIELDS_END = [
  "type",
  "source",
  "superoptions"
]

# Ignore sensors values with these suffixes -- they're static for this hardware (and often bogus or 100)
SENSORS_IGNORE_SUFFIX = [ '_min', '_max', '_crit', '_alarm', '_hyst', '_emergency', '_cap' ]
# Ignore these adapters.
SENSORS_IGNORE_ADAPTER_PREFIX = [ 'BAT' ] # Battery -- small subset

class sys(StatsModule):
  def __init__(self, daemon: StatsDaemon, api: StatsModuleAPI):
    self.daemon = daemon
    self.api = api
    self.last_stats = self.get_cpu_stats()

  # Gets desired/reported mount stats only for devices we want to report.  Currently: physical devices and tmpfs drives,
  # mostly mount point and space usage.
  def get_mount_stats(self):
    seen_shared = set()
    def filter_mount(mount):
      # We only want the first mount for each "shared" dynamic key -- rest are bind (or bind-like) mounts of the same FS
      shared = mount.get('dyn_shared', None)
      if shared in seen_shared:
        return False
      elif shared:
        seen_shared.add(shared)
        # /dev and tmpfs things
      if not mount.get('source', '').startswith("/dev") and not mount.get('type') == "tmpfs":
        return None
      return {
        "mountpoint": mount['mountpoint'],
        "available": mount['f_bavail'] * mount ['f_bsize'],
        "free": mount['f_bfree'] * mount ['f_bsize'],
        "size": mount['f_blocks'] * mount ['f_bsize'],
        "files": mount['f_files']
      }
    return filter(None, map(filter_mount, self.get_raw_mount_stats()))

  def get_nvme_stats(self):
    # This command must exactly match the sudoers.d file we shipped with to be allowed
    #
    # Also, it formats numbers as localized (e.g. comma-separated) strings obeying locale, in json, because reasons, so
    # block that.
    stats = util.get_process_output_json(["sudo", "/usr/bin/nvme", "smart-log", "/dev/nvme0", "-o", "json"], self.api,
                                         env=os.environ | { "LC_ALL": "C", "LANG": "C", "LC_NUMERIC": "C" })
    if not stats:
      return []

    # Convert all parsable values to int.  As of this writing they're all int, at least.  They used to be json numbers,
    # then they became localized strings, there's no promises here apparently.  The backend wont store mixed values that
    # don't match the preexisting ones.
    for k, v in stats.items():
      try:
        stats[k] = int(v)
      except ValueError:
        pass

    # Just nvme0 for now due to the sudo thing.  If we actually need multiple drives we should update that to some
    # `stats-getnvme <device>` helper script that can be elevated.
    return [{ "device": "nvme0", **stats }]

  def get_sensors_stats(self):
    sensors = util.get_process_output_json(["sensors", "-j"], self.api)

    # Parsed sensors data
    stats = []
    for adapter, groups in sensors.items():
      if True in (adapter.startswith(prefix) for prefix in SENSORS_IGNORE_ADAPTER_PREFIX):
        continue
      for groupname, group in groups.items():
        if type(group) is dict:
          for stat, value in group.items():
            if True in (stat.endswith(suffix) for suffix in SENSORS_IGNORE_SUFFIX):
              continue
            stats.append({ "adapter": adapter, "component": groupname, "stat": stat, "value": value })
    return stats

  # Get all mounts in /proc/self/mountinfo.  Maps fields to MOUNTINFO_FIELDS_START + MOUNTINFO_FIELDS_END.  "dynamic"
  # fields in /proc/self/mountinfo are mapped to "dyn_<key>"
  def get_raw_mount_stats(self):
    mountlines = open("/proc/self/mountinfo", "r", encoding="utf-8").read().strip().split("\n")
    # This format is a little weird - need to split off the first block of fields in MOUNTINFO_FIELDS_START, then find
    # the "-" separator, then map the next fields to MOUNTINFO_FIELDS_END -- and the in-between are dynamic fields of
    # key:value.  Ty kernel.
    mounts = []

    for strline in mountlines:
      line = strline.split(" ")
      # First chunk
      start = line[:len(MOUNTINFO_FIELDS_START)]
      # Remainder, find dash
      rest = line[len(MOUNTINFO_FIELDS_START):]
      dash_pos = rest.index("-")
      # Dynamic fields and second block of static fields
      end = rest[dash_pos+1:]
      dynamic = rest[:dash_pos]

      # Map static fields to defined keys
      start_dict = dict((MOUNTINFO_FIELDS_START[idx], start[idx]) for idx in range(len(MOUNTINFO_FIELDS_START)))
      end_dict = dict((MOUNTINFO_FIELDS_END[idx], end[idx]) for idx in range(len(MOUNTINFO_FIELDS_END)))
      # Map dynamic fields which are in key:value format prefixed with "dyn_"
      dynamic_dict = dict(("dyn_"+elem.split(":")[0], elem.split(":")[1]) for elem in dynamic)

      # Build object
      mount = { **start_dict, **end_dict, **dynamic_dict }

      # call statvfs to add usage info. All fields start with f_
      stat_result = None
      try:
        stat_result = os.statvfs(mount['mountpoint'])
      except:
        # This happens a lot with /run/ nonsense
        self.api.debug(f"Failed to get stats for mountpoint {mount['mountpoint']}")

      if stat_result:
        stat_dict = dict((key, getattr(stat_result, key)) for key in dir(stat_result) if key.startswith("f_"))
        mount.update(stat_dict)

      mounts.append(mount)

    return mounts

  def get_memory_stats(self):
    # Read lines from /proc/meminfo ignoring the trailing \n
    meminfo_lines = open("/proc/meminfo", "r", encoding="utf-8").read().strip().split("\n")
    # Split lines by 1+ spaces (format "Foo:      0 kb")
    parsed_lines = (re.split(RE_SPACES, line) for line in meminfo_lines)
    # Each line is either `key: int_value unit` or just `key: int_value`
    #  - ignore unit, remove ':', parse int
    return dict((line[0].rstrip(':'), int(line[1])) for line in parsed_lines)

  def get_battery_stats(self):
    stats = {}
    batteries = [*filter(os.path.exists, BATTERIES)]
    if not len(batteries):
      return {}

    for field in BATTERY_FIELDS.keys():
      ftype = BATTERY_FIELDS[field]

      try:
        dat = open(os.path.join(batteries[0], field), "rb").read().decode("utf-8")
        stats[field] = ftype(dat)
      except:
        self.daemon.log(f"sys: Missing or unparsable battery field: {field}")
        stats[field] = ftype(None)
    return stats

  # Parsed total elapsed CPU time from /proc/stat
  # Returns a dict with keys from PROC_COLUMNS and total-elapsed-time as values (int)
  def get_cpu_stats(self):
    stat_data = open("/proc/stat", "rb").read().decode("utf-8")
    stats = dict((x[0], x[1:]) for x in (re.split(RE_SPACES, stat) for stat in stat_data.split("\n")) if x[0])

    # Map columns to values, accounting for us knowing of more/less than this kernel reports

    # Get this stat index parsed to an int, or 0
    def statget(i):
      return int(stats['cpu'][i]) if i < len(stats['cpu']) else 0

    return dict((PROC_COLUMNS[i], statget(i)) for i in range(len(PROC_COLUMNS)))

  # Returns the delta-time since the last get_delta_cpu_stats call in a dict of e.g. { 'idle': 99.4, 'system': 0.6 }
  # Keys are defined in PROC_COLUMNS
  def get_delta_cpu_stats(self):
    new_stats = self.get_cpu_stats()

    # Get deltas
    delta_stats = dict((key, new_stats[key] - self.last_stats[key]) for key in new_stats.keys())
    # Sum up new elapsed samples
    total = sum(delta_stats.values())

    # Convert to % of total
    pct_stats = dict((key, delta_stats[key] / total) for key in delta_stats.keys())

    self.last_stats = new_stats
    return pct_stats

  # Collect the stat(s).
  #
  # WARNING: Do not do excessive blocking in this function -- running for longer than STATS_INTERVAL_JITTER will cause
  #   collections to be missed.  If you need to do blocking things to collect the stats, spawn a thread in __init__ and
  #   just have collect() return the most-recent collection.
  def collect(self):
    return [{ "operation": "cpu",     **self.get_delta_cpu_stats() },
            { "operation": "memory",  **self.get_memory_stats()    },
            { "operation": "battery", **self.get_battery_stats()   },
            *({ "operation": "disk",   **disk }   for disk   in self.get_mount_stats()),
            *({ "operation": "sensor", **sensor } for sensor in self.get_sensors_stats()),
            *({ "operation": "nvme",   **nvme }   for nvme   in self.get_nvme_stats()) ]
