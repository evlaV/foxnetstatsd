#!/bin/env python3
# -*- fill-column: 120; indent-tabs-mode: nil; tab-width: 10; python-indent-offset: 2 -*-

# Returns stats about network

from . import core
from .core import StatsModule, StatsDaemon, StatsModuleAPI

from pathlib import Path
from typing import Callable

class net(StatsModule):
  def __init__(self, daemon: StatsDaemon, api: StatsModuleAPI):
    pass

  # Collect the stat(s).
  #
  # WARNING: Do not do excessive blocking in this function -- running for longer than STATS_INTERVAL_JITTER will cause
  #   collections to be missed.  If you need to do blocking things to collect the stats, spawn a thread in __init__ and
  #   just have collect() return the most-recent collection.
  def collect(self):
    net_path = Path('/sys/class/net/')
    all_interfaces = [interface for interface in net_path.iterdir() if interface.is_dir()]
    interfaces = [interface for interface in all_interfaces if interface.name != 'lo']

    stats = [
      {
        # Name of the stat, for where it is bucketed
        "operation": "net",
        'name':       interface.name,
        'type':       read_int(interface / 'type'),
        'rx_bytes':   read_int(interface / 'statistics' / 'rx_bytes'),
        'rx_packets': read_int(interface / 'statistics' / 'rx_packets'),
        'tx_bytes':   read_int(interface / 'statistics' / 'tx_bytes'),
        'tx_packets': read_int(interface / 'statistics' / 'tx_packets'),
      }
      for interface in interfaces
    ]
    return stats

def read_scalar(path: Path, value_formatter: Callable[[str], any]):
  with path.open('r', encoding='utf-8') as node:
    value = node.read().strip()
  return value_formatter(value)

def read_int(path: Path):
  return read_scalar(path, int)
