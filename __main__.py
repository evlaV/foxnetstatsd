#!/bin/env python3
# -*- fill-column: 120; indent-tabs-mode: nil; tab-width: 10; python-indent-offset: 2 -*-

from . import core
import sys

if __package__ == '':
  import sys
  sys.stderr.write("ERR: foxnetstatsd not loaded as a module, (did you run 'python foxnetstatsd' rather than 'python -m foxnetstatsd'?)\n")
  sys.exit(1)

core.main_and_exit(sys.argv)
