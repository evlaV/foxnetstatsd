#!/bin/env python3
# -*- fill-column: 120; indent-tabs-mode: nil; tab-width: 10; python-indent-offset: 2 -*-

import sys
import sqlite3
import json
import importlib
import socket
import time
import gzip
import os
import asyncio
import aiohttp
from . import util

from functools import reduce
from pprint import pformat

DB_LOCATION = os.path.join( util.get_home_directory(), "foxnetstatsd.sqlite" )

METRICSFLUME_ENDPOINT = "http://10.77.77.1:2600/steam_metrics"

# How many metrics to submit per pass and the http timeout.
BATCH_SUBMIT_SIZE = 1000
BATCH_SUBMIT_TIMEOUT = 60.0
BATCH_CONNECT_TIMEOUT = 5.0

# Enabled stats modules
#
# A module must exist as an import-able submodule inside the foxnetstatsd module, and expose a StatsModule-derived class
# matching its name
#
# Example:
#   foxnetstatsd/gamescope.py, accessible via "from . import gamescope", providing "class gamescope(StatsModule)"
MODULES = [ "steamstatus", "sys", "gamescope", "startup", "net" ]

# Interval to align stats to, in seconds.
STATS_INTERVAL = 10.0
# Roughly how long we can over/undershoot a stats window before we skip it
# (best effort)
STATS_INTERVAL_JITTER = 0.1

# Stats that have failed to submit for this long should be written to disk
PERSIST_PERIOD = 15.0

# Check for persisting stats at most this often
PERSIST_BATCH_TIME = 30.0

# Maximum stats to persist to disk
PERSIST_STATS_LIMIT = 50000

# The schema for the local persistence database.
#
# These instructions always run unconditionally, followed by checking the schema version and running DB_SCHEMA_UPGRADES.
DB_SCHEMA = [ '''CREATE TABLE IF NOT EXISTS
                   "stats" ("id" INT NOT NULL UNIQUE,
                            "json" VARCHAR)''',
              '''CREATE TABLE IF NOT EXISTS
                   "keyvalues" ("key" VARCHAR NOT NULL UNIQUE,
                                "value" VARCHAR NOT NULL)'''
]

DB_SCHEMA_VER = 1

# Commands to run to go from previous schema version to this one
DB_SCHEMA_UPGRADES = {
  # v1: Changed version table around.  Renamed to "dbversion" from "version" so we'll just detect old schemas without a
  #     version table as v0 anyway, safe to drop.
  1: [ 'DROP TABLE IF EXISTS "version"' ]
  # v1.1: Added keyvalues table.  Don't actually need to update schema
}

# Internal persistence storage
#   Just dead-simple sqlite keyvalue storage so it can worry about the storage guarantees
class Persist:
  # Initialize a set of tables/schema for the given name.
  def initschema(self, schema_name, schema_version, schema, schema_upgrades):
    # Initialize schema
    self.db.execute("BEGIN")

    # Schema version
    self.db.execute('''CREATE TABLE IF NOT EXISTS
                         "dbversion" ("schema" VARCHAR NOT NULL UNIQUE,
                                      "dbver" INT NOT NULL)''')
    ver_cur = self.db.execute('''SELECT * FROM dbversion WHERE schema = ?''', (schema_name,))
    ver_row = ver_cur.fetchone()
    dbver = ver_row['dbver'] if ver_row else schema_version

    # "version 0" schema was potentially missing the version table, detect that we're actually v0 by existing table
    # 'stats'
    if not ver_row and schema_name == "master":
      cur = self.db.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='stats'")
      if cur.fetchone():
        # Actually a v0 schema.
        dbver = 0

    # Initial version if not set.
    if not ver_row:
      self.db.execute("INSERT INTO dbversion VALUES (?, ?)", (schema_name, dbver))

    # Now build the normal schema
    for sql in schema:
      self.db.execute(sql)

    # Upgrade it if needed
    while dbver < schema_version:
      nextver = dbver + 1
      self.daemon.log(f"Upgrading db schema from {dbver} to {nextver}")
      for sql in schema_upgrades[nextver]:
        self.db.execute(sql)
      self.db.execute("UPDATE dbversion SET dbver = ? WHERE schema = ?", (nextver, schema_name))
      dbver = nextver

    # Done
    self.db.commit()

  def __init__(self, daemon):
    self.db = sqlite3.connect(DB_LOCATION)
    self.db.row_factory = sqlite3.Row
    self.daemon = daemon

    self.initschema("master", DB_SCHEMA_VER, DB_SCHEMA, DB_SCHEMA_UPGRADES)

  # Load all persisted stat rows
  def load_all(self):
    cur = self.db.cursor()
    # Load up to PERSIST_STATS_LIMIT
    cur.execute("SELECT * FROM stats ORDER BY id DESC LIMIT ?", (PERSIST_STATS_LIMIT,))

    now = time.monotonic()
    ret = [ { "foxid": row['id'], "foxtime": now, 'data': json.loads(row['json']) } for row in cur.fetchall() ]
    # List is usually in ascending order, but we selected with DESC so we could limit it
    ret.reverse()

    # Drop any that exceded persist limit -- for older builds that persisted an inappropriate amount to disc
    if len(ret):
      dropped = self.drop_below_inclusive(ret[0]["foxid"] - 1)
      if dropped:
        self.daemon.log(f"Found more than {PERSIST_STATS_LIMIT} stored entries, dropped least-recent {dropped} records")

    return ret

  # Drop rows below this index, inclusive
  def drop_below_inclusive(self, idx):
    cur = self.db.cursor()
    cur.execute("DELETE FROM stats WHERE id <= ?", (idx,))
    self.db.commit()
    return cur.rowcount

  def add_stats(self, stats):
    cur = self.db.cursor()
    cur.executemany("INSERT INTO stats(id, json) VALUES (?, ?)",
                    ((stat['foxid'], json.dumps(stat['data'])) for stat in stats))
    self.db.commit()
    return cur.rowcount

# per-module API class we give them to store k/vs and log and whatever they want idk
class StatsModuleAPI:
  def __init__(self, modname, daemon):
    self.daemon = daemon
    self.modname = modname
    self._cache = {}

  def debug(self, msg):
    self.daemon.debug(f"{self.modname}: {msg}")

  def verbose(self, msg):
    self.daemon.verbose(f"{self.modname}: {msg}")

  def log(self, msg):
    self.daemon.log(f"{self.modname}: {msg}")

  def persist_set(self, rawkey: str, rawvalue):
    key = f"{self.modname}.{rawkey}"
    value = json.dumps(rawvalue)
    self.daemon.debug(f"persist_set: {key}: {value}")

    cur = self.daemon.persist.db.cursor()
    cur.execute('''INSERT INTO keyvalues(key, value) VALUES(?, ?)
                   ON CONFLICT(key) DO UPDATE SET value = ?''', (key, value, value))
    self.daemon.persist.db.commit()
    self._cache[key] = value

  def persist_get(self, rawkey):
    key = f"{self.modname}.{rawkey}"
    if not key in self._cache:
      cur = self.daemon.persist.db.cursor()
      cur.execute("SELECT key, value FROM keyvalues WHERE key = ?", (key,))
      row = cur.fetchone()
      self._cache[key] = row['value'] if row else 'null'

    return json.loads(self._cache[key])

##
## Main stats daemon class
##
class StatsDaemon:
  def debug(self, obj):
    if self.debug_flag: self.log(obj)

  def verbose(self, obj):
    if self.verbose_flag: self.log(obj)

  def debug_dump_list(self, listobj):
    # Don't pretty format big lists if we're not going to log them
    if not self.debug_flag:
      return

    # Don't prettyprint/log 100,000 entries though
    limit = 100 # Should be even
    if len(listobj) > limit:
      head = pformat(listobj[0:(limit//2)])
      tail = pformat(listobj[-(limit//2):])
      return self.debug(f"{head}\n ... [{len(listobj) - limit} items not shown] ...\n{tail}")
    else:
      return self.debug(pformat(listobj))

  def log(self, obj):
    print(f"{obj}", file=sys.stderr, flush=True)

  def __init__(self, verbose = False, debug = False):
    self.persist = Persist(self)
    self.pass_counter = 0 # FIXME
    self.mods = {}
    self.submit_task = None
    self.debug_flag = debug
    self.verbose_flag = verbose or debug
    # Prefer configured hostname on disk to active hostname, mostly because some imaged units only opportunistically set
    # their hostname during bootstrapping and wont have it active on first boot
    try:
      self.host = open("/etc/hostname", "r", encoding="utf-8").read().strip()
    except:
      self.host = socket.gethostname()
    self.last_pass = time.time()
    self.last_persist = time.monotonic()
    self.log(f"Starting on {self.host} with modules {MODULES}")
    for modname in MODULES:
      mod = importlib.import_module(f".{modname}", __package__)
      modclass = mod.__dict__[modname]
      # Spawn module with a pointer to ourselves and a ModulePersist instance
      inst = modclass(self, api=StatsModuleAPI(modname, self))
      assert isinstance(inst, StatsModule), f"{type(inst)} does not inherit from {StatsModule}"
      self.mods[modname] = inst

    # Load existing unsubmitted stats, find the highest ID to start ordering after it
    self.pending_stats = self.persist.load_all()
    self.top_foxid = reduce(max, (stat['foxid'] for stat in self.pending_stats)) if len(self.pending_stats) else 0
    self.persisted_foxid = self.top_foxid
    self.log(f"Loaded {len(self.pending_stats)} pending stats from database, high ID is now {self.top_foxid}")
    self.debug_dump_list(self.pending_stats)

  # Assign the next incrementing stat index
  def assign_idx(self):
    self.top_foxid += 1
    return self.top_foxid

  # Append a simple stat from module "session" with the current time and metadata about what's happening
  # Used for session start/stop/heartbeat as desired
  def add_session_stat(self, key, data = {}):
    stat = { "operation": key, "pending_stats": len(self.pending_stats), **data }
    stat = self.build_stat("session", time.time(), stat)
    self.pending_stats.append(stat)

  # Persist pending stats older than some time.  If save_all is set, persist all unsubmitted stats
  def persist_pending(self, save_all = False):
    start_idx = 0
    stop_idx = len(self.pending_stats)

    # Find the first unpersisted id from the foxid high water mark
    for idx in range(len(self.pending_stats)):
      if self.pending_stats[idx]['foxid'] > self.persisted_foxid:
        start_idx = idx
        break
    else:
      # None found
      return

    # Find the last index that is old enough to persist
    mono_older_than = time.monotonic() - PERSIST_PERIOD

    # If save_all we'll save everything
    if not save_all:
      for idx in range(start_idx, len(self.pending_stats)):
        if self.pending_stats[idx]['foxtime'] > mono_older_than:
          stop_idx = idx
          break

    save_slice = self.pending_stats[start_idx:stop_idx]

    # Save those tats
    if save_slice:
      # Mark new high water mark
      self.debug("Persisting pending stats:")
      self.debug_dump_list(save_slice)
      written = self.persist.add_stats(save_slice)
      assert written == len(save_slice)
      new_persist_id = save_slice[-1]['foxid']
      self.verbose(f"Persisted {written} old stats to database.  Oldest stat is from {self.pending_stats[0]['data']['epoch']}")
      self.verbose(f"Persisted stats increased from {start_idx} -> {stop_idx}")
      self.verbose(f"Persisted water mark changed from {self.persisted_foxid} -> {new_persist_id}")
      self.persisted_foxid = new_persist_id

  def shutdown(self):
    self.log("Shutting down")
    for modname,mod in self.mods.items():
      mod.shutdown()
    self.mods = None
    # Create a shutdown stat event
    self.add_session_stat("stop")
    # Save all unpersisted stats for shutdown
    self.persist_pending(save_all = True)

  # Run the stats daemon.
  def run(self):
    asyncio.run(self._run_async())

  # Run the stats daemon as a task.  Wrapper for ensuring shutdown() gets called around actual _run() inner loop.
  #
  # Private, as we create other tasks that we're relying on the event loop shutting down to cancel.  Being part of a
  # bigger event loop might do weird things.
  async def _run_async(self):
    try:
      await self._run()
    finally:
      self.shutdown()

  async def _run(self):
    # Create a startup stat event
    self.add_session_stat("start")

    while True:
      pass_time = await self.sleep_until_next_pass()
      self.collect_stats(pass_time)
      self.last_pass = pass_time
      self.pass_counter += 1

      # Heartbeat
      self.add_session_stat("heartbeat", data = { "last_pass": pass_time })

      # Rejoin previous task if done
      if self.submit_task and self.submit_task.done():
        await self.submit_task
        self.submit_task = None

      # Drop excess stats only between submit_task runs since the submit task also is culling from the bottom of the
      # list
      if not self.submit_task:
        excess_stats = len(self.pending_stats) - PERSIST_STATS_LIMIT
        if excess_stats > 0:
          self.log(f"More than {PERSIST_STATS_LIMIT} stats in queue ({len(self.pending_stats)}), dropping oldest")
          self.delete_lowest_pending_stats(excess_stats)

      # Kickoff new submit task if not running and stats are pending
      if len(self.pending_stats) and not self.submit_task:
        self.submit_task = asyncio.create_task(self.task_submit_stats())

      # Save any stats that are past the PERSIST_PERIOD timeout to disk
      if time.monotonic() - self.last_persist >= PERSIST_BATCH_TIME:
        self.persist_pending()
        self.last_persist = time.monotonic()

  async def metricsflume_submit(self, stats):
    data = ("\n".join(json.dumps(stat['data']) for stat in stats) + "\n").encode("utf-8")
    usize = len(data)
    data = gzip.compress(data)
    self.verbose(f"metricsflume: Submitting payload, ({usize} -> {len(data)} bytes)")
    res = False
    try:
      timeout = aiohttp.ClientTimeout(total=BATCH_SUBMIT_TIMEOUT, connect=BATCH_CONNECT_TIMEOUT)
      async with aiohttp.ClientSession(headers={'Content-Encoding': 'gzip'}, timeout=timeout) as session:
        async with session.post(METRICSFLUME_ENDPOINT, data=data) as resp:
          res = resp.status
    except Exception as e:
      self.verbose(f"metricsflume: Got {type(e)} exception while submitting: {e}")
    self.verbose(f"metricsflume: Response was: {res}")
    return res == 200

  # Delete the lowest N stats from the pending stats array, and flush any persisted stats below our new lowest pending
  # stat
  def delete_lowest_pending_stats(self, count):
    dropped = self.pending_stats[:count]
    if len(dropped) == 0:
      return

    lowest_id_dropped = dropped[0]['foxid']
    highest_id_dropped = dropped[-1]['foxid']

    self.pending_stats = self.pending_stats[count:]
    self.verbose(f"Dropped {len(dropped)} pending stats")

    # Drop any submitted stats that had been persisted.  Can skip this (and the disk access) if the persisted high water
    # mark is below our stats range, implying none are currently persisted.
    if lowest_id_dropped <= self.persisted_foxid:
      persist_dropped = self.persist.drop_below_inclusive(highest_id_dropped)
      self.verbose(f"Dropped {persist_dropped} persisted stats")

  async def task_submit_stats(self):
    # Try to submit stats.  Fine if it fails, we save and keep trying
    if not len(self.pending_stats):
      return False

    # Submit BATCH_SUBMIT_SIZE stats at a time
    while len(self.pending_stats):
      stats = self.pending_stats[:BATCH_SUBMIT_SIZE]
      self.verbose(f"Sending {len(stats)} stats")
      self.debug_dump_list(stats)

      if not await self.metricsflume_submit(stats):
        self.log(f"Failed to submit {len(stats)} stats, assuming no connection. {len(self.pending_stats)} pending.")
        return False

      # Clear all pending and drop any that were persisted
      self.delete_lowest_pending_stats(len(stats))
      self.log(f"Submitted {len(stats)} stats, {len(self.pending_stats)} pending")

    return True

  # Round a unix timestamp to the STATS_INTERVAL timed window it is in (the start of the window)
  def round_time_to_pass(self, timestamp):
    return round(timestamp / STATS_INTERVAL, 0) * STATS_INTERVAL

  # Sleep until we're at the next stats window, return the timestamp we should use for said pass
  async def sleep_until_next_pass(self):
    while True:
      next_pass = self.round_time_to_pass(self.last_pass) + STATS_INTERVAL
      now = time.time()
      if next_pass < now:
        if self.pass_counter > 0:
          self.log(f"Missed pass! Next pass should be at {next_pass} but it is now {now}")
        next_pass = self.round_time_to_pass(now) + STATS_INTERVAL

      # Try to sleep until desired time
      await asyncio.sleep(next_pass - now)
      now = time.time()

      # Check that we woke up at roughly the right time.
      if abs(next_pass - now) <= STATS_INTERVAL_JITTER:
        return next_pass
      else:
        # Complain and loop.  This should happen sometimes if the system clock moves or we sleep/etc.  If it is
        # happening during normal usage, jitter might need to be increased for this system.
        self.log(f"Missed pass - overshot while sleeping, tried to wake up at {next_pass} but it is now {now}")

  def build_stat(self, modulename, pass_timestamp, data):
    assert type(data) is dict and 'operation' in data, "Module {modulename} returned malformed stats"

    stat = { 'foxid': self.assign_idx(),
             # How long this has been queued for this session, separate from the measurement timestamp.  Always
             # monotonic.
             'foxtime': time.monotonic(),
             'data': data }

    data['catalog'] = f"foxstats.{modulename}"
    data['host'] = self.host
    # We're submitting this at-some-point so it needs a timestamp of when we collected it
    # Alias 'time' into epoch.
    data['epoch'] = data.get('epoch', data.get('time', pass_timestamp))
    return stat

  # Grab a round of stats from all mods.  Takes a timestamp for the official time the pass started.
  def collect_stats(self, pass_timestamp):
    new_stats = []
    for name, mod in self.mods.items():
      stats = mod.collect()
      if stats:
        # Namespace each measurement and give it an ID for persistence tracking
        batch = [self.build_stat(name, pass_timestamp, stat) for stat in stats]
        new_stats.extend(batch)

    # Add
    self.pending_stats.extend(new_stats)
    self.verbose(f"Collected {len(new_stats)} stats from {len(self.mods)} modules")

# Abstract stats module the others should inherit
class StatsModule:
  def __init__(self, daemon: StatsDaemon, api: StatsModuleAPI): raise "Instantiating abstract class"
  def collect(self): raise Exception(f"{type(self)} must implement collect to be used as a stats module")
  def shutdown(self): pass # Optional

##
## Main functions
##
def main(args=[]):
  d = StatsDaemon(debug=('debug' in args), verbose=('verbose' in args))
  d.run()

def main_and_exit(args=[]):
  main(args)
  sys.exit(0)
