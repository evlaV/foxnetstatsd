#!/bin/env python3
# -*- fill-column: 120; indent-tabs-mode: nil; tab-width: 10; python-indent-offset: 2 -*-

# Common utilities
import json
import subprocess
import os

# Returns the current user's home directory or raises a generic Exception
def get_home_directory():
  # os.path.expanduser is the only standard library function to do home lookup properly, even though this looks like
  # abusing some shell parsing feature.
  home = os.path.expanduser("~")
  # But it just returns the input string if it couldn't do the lookup.
  # If your home directory is literally the working-directory-relative string '~', well, blame python. And yourself.
  if home == "~":
    raise Exception("Could not determine current home directory")
  return home

# Returns a (string) process output, or None for failure
def get_process_output(cmd, logapi, env=None):
  cmdname = cmd[0]
  ret = subprocess.run(cmd, capture_output=True, env=env)
  if ret.returncode != 0:
    errstr = ""
    try:
      errstr = ": " + ret.stderr.decode("utf-8")
    except:
      pass
    logapi.log(f"command {cmd} returned failure {ret.returncode}{errstr}")
    return None
  try:
    return ret.stdout.decode("utf-8")
  except:
    logapi.log(f"command {cmd} returned invalid utf-8")
    return None

# Returns a json-object parsed from a process's output, or None for failure
def get_process_output_json(cmd, logapi, env=None):
  ret = get_process_output(cmd, logapi, env)
  if ret is None:
    return ret
  try:
    return json.loads(ret)
  except:
    logapi.log(f"command {cmd[0]} returned invalid JSON: {ret}")
    return None
