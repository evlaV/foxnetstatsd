#!/bin/env python3
# -*- fill-column: 120; indent-tabs-mode: nil; tab-width: 10; python-indent-offset: 2 -*-

# Startup collector -- stats that only need to report once per run

from . import core, util
from .core import StatsModule, StatsDaemon, StatsModuleAPI

import subprocess
import os
import json
import re

DMI_PATH="/sys/class/dmi/id"
ESP_DMI_PATHS=[ "/boot/efi/foxnet_dmi.json", "/esp/foxnet_dmi.json" ]

class startup(StatsModule):
  def __init__(self, daemon: StatsDaemon, api: StatsModuleAPI):
    # Do whatever setup we want.
    self.daemon = daemon
    self.api = api
    self.initial = False # Initial collection occured

  def readfile(self, path):
    dat = None
    try:
      dat = open(path, "rb").read().decode("utf-8").strip()
    except PermissionError:
      # Legit.  This needs to be a proper privileged helper for non-dev units
      self.api.verbose(f"Cannot read {path} normally, trying janky sudo thing")
      ret = subprocess.run([ "sudo", "/usr/bin/cat", path ], capture_output=True)
      if ret.returncode == 0:
        try:
          dat = ret.stdout.decode("utf-8").strip()
        except:
          pass
      if dat is None:
        self.api.verbose(f"Couldn't read {path} even with sudo jank")
    except Exception as e:
      self.api.log(f"Got exception trying to read DMI key '{path}': {type(e)}: '{e}'")
    return dat

  def get_dmi_fields(self):
    try:
      files = os.listdir(DMI_PATH)
    except:
      self.api.verbose(f"Cannot read {DMI_PATH}")
      return {}

    ret = {}

    for dmi in files:
      path = os.path.join(DMI_PATH, dmi)
      if os.path.isfile(path):
        dat = self.readfile(path)
        if dat:
          ret[dmi] = dat

    return ret

  def get_esp_saved_fields(self):
    dat = None
    for path in ESP_DMI_PATHS:
      try:
        dat = json.loads(self.readfile(path))
        return dict((f"esp.{field}", value) for (field, value) in dat.items())
      except:
        continue

    # Couldn't open any
    self.api.log(f"Couldn't load any saved DMI information from paths: {ESP_DMI_PATHS}")
    return {}

  def get_controller_info(self):
    dat = None
    try:
      dat = util.get_process_output_json(
            ["/usr/share/jupiter_controller_fw_updater/d21bootloader16.py","getdevicesjson",],
            self.api)
      controller = None
      if len(dat):
        controller = dat[0]
        controller["other_controller_count"] = len(dat) - 1
      return dict(
        (f"controller.{field}", value) for (field, value) in controller.items())
    except:
      self.api.log("Failed to read controller info")
      return {}

  def get_os_info(self):
    try:
      dat = self.readfile("/etc/os-release")
      linepattern = r'^([^=]+)="?(.*?)"?$'
      dat = dict(re.match(linepattern, item).groups() for item in dat.split("\n"))
      return dict((f"steamos.{field}", value) for (field, value) in dat.items())
    except:
      self.api.log("Failed to read SteamOS info")
      return {}


  # Collect the stat(s).
  #
  # WARNING: Do not do excessive blocking in this function -- running for longer than STATS_INTERVAL_JITTER will cause
  #   collections to be missed.  If you need to do blocking things to collect the stats, spawn a thread in __init__ and
  #   just have collect() return the most-recent collection.
  def collect(self):
    # We report stats once per go
    if self.initial:
      return []

    self.initial = True
    return [{ "operation": "state",
              **self.get_dmi_fields(),
              **self.get_esp_saved_fields(),
              **self.get_controller_info(),
              **self.get_os_info() }]
