#!/bin/env python3
# -*- fill-column: 120; indent-tabs-mode: nil; tab-width: 10; python-indent-offset: 2 -*-

# Monitors gamescope's stats pipe

from . import core
from .core import StatsModule, StatsDaemon, StatsModuleAPI

import psutil
import threading
import os
import select
import statistics

XDG_RUNTIME_DIR = os.environ['XDG_RUNTIME_DIR'] if 'XDG_RUNTIME_DIR' in os.environ else None
GAMESCOPE_SOCKET = os.path.join(XDG_RUNTIME_DIR, 'gamescope-stats', 'stats.pipe') if XDG_RUNTIME_DIR else None

class gamescope(StatsModule):
  def __init__(self, daemon: StatsDaemon, api: StatsModuleAPI):
    self.daemon = daemon
    self.api = api
    self.gamescope_process = None
    self.gamescope_thread_pipe = None # Used to signal the thread to stop, as well as for the thread to check that it
                                      # wasn't pre-empted
    self.gamescope_thread = None
    self.fps_values = []
    self.fps_last_median = None # gamescope reports this data very slowly, hold the last value submitted

    self.check_gamescope_thread()

  def shutdown(self):
    self.clear_thread()

  def clear_thread(self):
    oldpipe = self.gamescope_thread_pipe
    oldthread = self.gamescope_thread

    # Clear these at once, the thread uses them being set as indicator it didn't race
    self.gamescope_thread_pipe = None
    self.gamescope_process = None
    self.gamescope_thread = None

    if oldpipe:
      os.close(oldpipe[0]) # Close our end of the pipe so the thread knows to go away
    if oldthread:
      oldthread.join() # Wait for it to do so

  # Find gamescope and its stats socket.  Returns tuple of ( proc, socket )
  def find_gamescope(self):
    proc = None
    for p in psutil.process_iter(attrs=['name']):
      if p.info['name'] == "gamescope":
        proc = p
        break

    if not proc:
      return (None,None)

    # Find socket
    sock = GAMESCOPE_SOCKET if GAMESCOPE_SOCKET is not None and os.path.exists(GAMESCOPE_SOCKET) else None

    if sock:
      return (proc, sock)

    # If we have a process but the socket isn't there, do a janky lookup for the old-style socket
    self.api.verbose("Found gamescope process but no socket, looking for old-style socket")
    candidate = None
    candidate_time = 0
    for tmpfile in os.listdir("/tmp"):
      if tmpfile.startswith("gamescope."):
        pipe = os.path.join("/tmp", tmpfile, "stats.pipe")
        if os.path.isdir(pipe):
          continue

        try: stat = os.stat(pipe)
        except: continue

        if stat.st_ctime > candidate_time:
          self.api.verbose(f"Found old-style socket: {pipe}")
          candidate = pipe
          candidate_time = stat.st_ctime

    # candidate might still be None
    return ( proc, candidate )

  # Check if the gamescope process has exited or restarted, and spawn a thread to monitor its stats pipe
  def check_gamescope_thread(self):
    if self.gamescope_process and self.gamescope_process.is_running():
      return

    # End old thread
    self.clear_thread()

    # Flush stats when clearing thread
    self.fps_last_median = None
    self.fps_values.clear()

    # Check for new gamescope and spawn a new thread if needed
    ( proc, stats_pipe ) = self.find_gamescope()
    if proc and stats_pipe:
      self.api.log(f"Found new gamescope process {proc.pid} with stats socket {stats_pipe}")
      (pipea, pipeb) = os.pipe()
      def threadstart(*args, **kwargs):
        return self.thread_main(*args, **kwargs)
      thread = threading.Thread(target=threadstart, kwargs={"end_pipe": pipeb, "stats_file": stats_pipe})
      self.gamescope_process = proc
      self.gamescope_thread = thread
      self.gamescope_thread_pipe = (pipea, pipeb)
      thread.start()

  # gamescope thread that tries to read newline-delimited bytes from the stats file, and passes them to
  # thread_parse_stats.  Watches for EOF and re-opens the file, or the end_pipe to close and exits
  def thread_main(self, stats_file, end_pipe):
    # Canceled before we ran?
    if not self.gamescope_thread_pipe or self.gamescope_thread_pipe[1] != end_pipe:
      return

    self.daemon.log(f"gamescope: Starting collection for process {self.gamescope_process.pid}")

    stats = None
    buf = bytes()
    while True:
      timeout = None
      if not stats:
        try:
          stats = os.open(stats_file, os.O_RDONLY | os.O_NONBLOCK)
          self.daemon.log(f"gamescope: Connected to new stats pipe {stats_file} ({stats})")
        except:
          # Just select on the pipe for a little bit
          self.daemon.log(f"gamescope: Couldn't connect to stats pipe, retrying ({stats_file})")
          timeout = 5

      rpipes = [end_pipe, stats] if stats else [end_pipe]
      (ready_read, ready_write, ready_x) = select.select(rpipes,[],[], timeout)
      # End pipe is only for terminate signal
      if end_pipe in ready_read:
        self.daemon.log("gamescope: Thread exiting")
        if stats:
          os.close(stats)
          os.close(end_pipe)
        return

      if not stats:
        continue

      # Try to read some stats
      newdata = os.read(stats, 1024)
      if len(newdata):
        buf += newdata
        loc = buf.rfind(b'\n')
        if loc != -1:
          self.thread_parse_stats(buf[:loc])
          buf = buf[loc+1:]
      else:
        # eof, reset and loop
        self.daemon.log(f"gamescope: Got EOF, re-opening file")
        os.close(stats)
        stats = None

  # Called on the thread with blocks of bytes pulled from the stats pipe.  thread_main only sends blocks that have
  # reached a newline (but there may be inline newlines in the block)
  def thread_parse_stats(self, stat_bytes):
    parsed = None
    try:
      # This should blow up if not utf-8, or not pairs-of-keyvalues-separated-by-\n
      parsed = dict(stat.split("=") for stat in stat_bytes.decode("utf-8").split("\n"))
    except Exception as e:
      self.daemon.log(f"gamescope: Got invalid utf-8 from stats pipe, ignoring")
      return

    for (key, val) in parsed.items():
      # For now, we only handle fps.  Build a list so we can median it in collect()
      if key == "fps":
        try:
          self.fps_values.append(float(val))
        except Exception as e:
          self.daemon.log(f"gamescope: Bad fps value from gamescope: {val}")

  # Returns the median fps of values since the last call, or None if no values have arrived in that time
  def get_delta_median_fps(self):
    if not self.gamescope_process:
      return None

    if not len(self.fps_values):
      return self.fps_last_median

    median = statistics.median(self.fps_values)
    self.fps_values.clear()
    self.fps_last_median = median
    return median

  # Collect the stat(s).
  #
  # WARNING: Do not do excessive blocking in this function -- running for longer than STATS_INTERVAL_JITTER will cause
  #   collections to be missed.  If you need to do blocking things to collect the stats, spawn a thread in __init__ and
  #   just have collect() return the most-recent collection.
  def collect(self):
    self.check_gamescope_thread()
    return [ { "operation": "stats", "median_fps": self.get_delta_median_fps() } ]
