#!/bin/env python3
# -*- fill-column: 120; indent-tabs-mode: nil; tab-width: 10; python-indent-offset: 2 -*-

# Example stats collector that just returns a counter that goes up

from . import core
from .core import StatsModule, StatsDaemon, StatsModuleAPI

class example(StatsModule):
  def __init__(self, daemon: StatsDaemon, api: StatsModuleAPI):
    # Do whatever setup we want.
    self.daemon = daemon
    self.counter = 1
    self.api = api

  # Collect the stat(s).
  #
  # WARNING: Do not do excessive blocking in this function -- running for longer than STATS_INTERVAL_JITTER will cause
  #   collections to be missed.  If you need to do blocking things to collect the stats, spawn a thread in __init__ and
  #   just have collect() return the most-recent collection.
  def collect(self):
    # Returns one stat, but could be an array of stats with different names.
    stats = [
      {
        # Name of the stat, for where it is bucketed
        "operation": "test_stat",
        # Timestamp this stat represents
        #   Optional, the daemon will provide a default value.  Useful if you're collecting stats asynchronously
        #   in a thread, and the actual timestamp of the stat isn't now.
        "epoch": time.time(),
        # One or more parameters this stat is recording for this timestamp.  (The "host" parameter is auto-filled by the
        # daemon to our hostname)
        "counter": self.counter
      },

      # [...] more stats here if needed
    ]
    self.counter += 1
    return stats
