#!/bin/env python3
# -*- fill-column: 120; indent-tabs-mode: nil; tab-width: 10; python-indent-offset: 2 -*-

from . import core
from .core import ( StatsDaemon, main, main_and_exit )

# __all__ = [  ]
